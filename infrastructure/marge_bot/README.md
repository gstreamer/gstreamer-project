This is the script used to bootstrap all the instances of the [marge_bot][1] to manage all the Merge Requests in GStreamer.

Marge only ever looks at a single project at a time and waits till the given Pipeline is finished,
which means that a single MR block all the other projects. To work around this we create many
instances of Marge where each one is responsible for a given module.

The script expects a `$PWD/secrets` folder, where the ssh key and gitlab token for the bot are expected to be in.
By default the filenames for the secrets are `marge-bot.token` for the gitlab token and `marge-bot-ssh-key` for the ssh key.
The folder which the script is run from, will be mounted inside all the containers created, so take care.

[1]: https://github.com/smarkets/marge-bot
