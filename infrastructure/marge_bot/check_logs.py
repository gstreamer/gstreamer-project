import os
from subprocess import call

MODULES = [
    "batch",
    "gst-integration-testsuites",
    "gst-plugins-rs",
    "gstreamer-rs",
    "meson_ports",
    "gstreamer-1.20",
    "gstreamer-1.18",
    "gstreamer-main",
    "cerbero-1.20",
    "cerbero-1.18",
    "cerbero-main",
]


def main():
    for module in MODULES:
        cmd = f"docker logs --tail 20 {module}"
        ret = call(cmd, shell=True)
        assert ret == 0


main()
