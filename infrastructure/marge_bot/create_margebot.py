import os
from subprocess import call

VERSION = "0.10.1"

RETIRED_BRANCHES = [
    "1.16",
    "1.18",
    # ..
]

ACTIVE_BRANCHES = [
    "main",
    "1.20",
    "1.22",
    # ..
]

# Dict of the container name: with a tuple of project_regex and a list of the active branches
MODULES = {
    "cerbero": ("gstreamer/cerbero", ACTIVE_BRANCHES),
    "gstreamer": ("gstreamer/gstreamer", ACTIVE_BRANCHES),
    # Catchall for the meson-ports since its low activity
    "meson_ports": ("gstreamer/meson-ports/.*", None),
    "gstreamer-rs": ("gstreamer/gstreamer-rs", None),
    "gst-plugins-rs": ("gstreamer/gst-plugins-rs", None),
    "gst-integration-testsuites": ("gstreamer/gst-integration-testsuites", None),
    # Batch the rest projects
    "batch": ("gstreamer/gst-indent|gstreamer/gst-template|gstreamer/gstreamer-project|gstreamer/orc|gstreamer/www", None),
}

# can fail if container name doesn't exist yet, should be fine
def remove_all():
    for name, (_project_regex, branches) in MODULES.items():
        if branches is not None:
            for branch in branches + RETIRED_BRANCHES:
                call(f"docker stop {name}-{branch}", shell=True)
                call(f"docker rm {name}-{branch}", shell=True)
        else:
            call(f"docker stop {name}", shell=True)
            call(f"docker rm {name}", shell=True)

def create_marge_instances():
    for name, (project_regex, branches) in MODULES.items():
        if branches is not None:
            for branch in branches:
                cmd = f"docker run -dt --restart=always --name='{name}-{branch}' -v $PWD:/configuration smarkets/marge-bot:{VERSION} --config-file=/configuration/config.yml --project-regex='{project_regex}$' --branch-regexp='{branch}'"
                ret = call(cmd, shell=True)
                assert ret == 0
        else:
            cmd = f"docker run -dt --restart=always --name='{name}' -v $PWD:/configuration smarkets/marge-bot:{VERSION} --config-file=/configuration/config.yml --project-regex='{project_regex}$'"
            ret = call(cmd, shell=True)
            assert ret == 0


def main():
    if not os.path.exists("secrets"):
        print("Please create a 'secrets' directory")
        exit(1)

    # cleanup existing containers
    remove_all()

    # create the new containers
    create_marge_instances()


main()
