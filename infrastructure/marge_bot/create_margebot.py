import os
from subprocess import call

MODULES = [
    "cerbero",
    "gst-build",
    "gst-ci",
    "gst-devtools",
    "gst-docs",
    "gst-editing-services",
    "gst-examples",
    "gst-integration-testsuites",
    "gst-libav",
    "gst-omx",
    "gst-plugins-bad",
    "gst-plugins-base",
    "gst-plugins-good",
    "gst-plugins-rs",
    "gst-plugins-ugly",
    "gst-python",
    "gstreamer",
    "gstreamer-project",
    "gstreamer-rs",
    "gstreamer-sharp",
    "gstreamer-vaapi",
    "gst-rtsp-server",
    "gst-template",
    "orc",
    "www",
]

MESON_PORTS = [
    "ffmpeg",
    "x264",
    "libffi",
    "gl-headers",
]

TEMPLATE = "template.yml"
VERSION = "0.10.0"

def concat_modules():
    containers = " ".join(str(x) for x in MODULES + MESON_PORTS)
    return containers

# can fail if container name doesn't exist yet, should be fine
def stop_all():
    containers = concat_modules();
    cmd = f"docker stop {containers}"
    call(cmd, shell=True)

# can fail if container name doesn't exist yet, should be fine
def remove_all():
    containers = concat_modules();
    cmd = f"docker rm {containers}"
    call(cmd, shell=True)

def create_container(name, module):
    cmd = f"docker run -dt --restart=always --name={name} -v $PWD:/configuration smarkets/marge-bot:{VERSION} --config-file=/configuration/config.yml --project-regex='gstreamer/{module}$'"
    ret = call(cmd, shell=True)
    assert ret == 0

def main():
    if not os.path.exists('secrets'):
        print("Please create a 'secrets' directory")
        exit(1)

    # cleanup existing containers
    stop_all()
    remove_all()

    # create the new containers
    for module in MODULES:
        create_container(module, module)

    for name in MESON_PORTS:
        create_container(name, f"meson-ports/{name}")

main()
