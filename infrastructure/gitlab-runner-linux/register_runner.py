#! /usr/bin/env python3


import argparse
import os
import socket
import subprocess


# Adapted from
# https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/blob/29f920969e4ed4ab0bcfff911e90cfe13562c5ef/cloud-init/ci/runcmd.d/99-gitlab-runner-register.gotmpl
def create_register_command(
    name: str,
    registration_token: str,
    concurrent: int,
    runner_limit: int,
    gitlab_url: str,
):
    # use sudo to register the runner system-wide
    register_command = ["sudo", "gitlab-runner", "register"]
    register_command += ["--name", f"{name}"]
    register_command += ["--non-interactive"]

    if runner_limit:
        register_command += ["--limit", f"{runner_limit}"]
        register_command += ["--request-concurrency", "2"]

    register_command += ["--output-limit", "32768"]
    register_command += ["--env", f"FDO_CI_CONCURRENT={concurrent}"]

    register_command += ["--executor", "docker"]
    register_command += ["--docker-host", "unix:///run/podman/podman.sock"]
    register_command += ["--docker-pull-policy", "if-not-present"]
    register_command += ["--docker-image", "debian:latest"]
    register_command += ["--docker-privileged=false"]

    # We run unpriv, but handy to have available
    # register_command += ["--docker-cap-add", "CAP_SYS_CHROOT"]
    # register_command += "--docker-privileged"
    # register_command += "--docker-devices /dev/kvm"

    register_command += ["--docker-volumes", "/var/cache/gitlab-runner/cache:/cache"]
    register_command += ["--docker-volumes", "/var/host:/host:ro"]
    register_command += ["--docker-disable-cache=true"]
    register_command += ["--env", "DOCKER_TLS_CERTDIR="]
    register_command += ["--docker-tmpfs", "/tmp:rw,nosuid,nodev,exec,mode=1777"]
    # Block reverse ssh
    register_command += ["--docker-extra-hosts", "ssh.tmate.io:192.0.2.1"]

    register_command += ["--registration-token", f"{registration_token}"]
    register_command += ["--url", f"{gitlab_url}"]

    if gitlab_url == "https://gitlab.freedesktop.org":
        gating_script = "https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/runner-gating/runner-gating.sh"
        curl_cmd = "/host/bin/curl -s -L --cacert /host/ca-certificates.crt --retry 4 -f --retry-delay 60"

        register_command += [
            "--pre-get-sources-script",
            f"{curl_cmd} {gating_script} | sh -s -- pre_get_sources_script",
        ]
        register_command += ["--pre-build-script", f"{curl_cmd} {gating_script}  | sh"]

    return register_command


def main():
    ncpus = os.cpu_count()
    print(f"Number of CPUs: {ncpus}")
    hostname = socket.gethostname()

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--name",
        "--runner-name",
        metavar="RUNNER_NAME",
        help="The name of the runner instance that will be displayed in gitlab. Defaults to the system hostname.",
        default=hostname,
    )
    parser.add_argument(
        "--runner-concurrency",
        type=int,
        default=0,
        metavar="RUNNER_LIMIT",
        help="The [[runners]].limit value",
    )
    parser.add_argument(
        "--build-concurrency",
        type=int,
        required=True,
        help="The value that will be set for FDO_CI_CONCURRENT",
    )
    parser.add_argument(
        "--registration-token",
        required=True,
        metavar="CI_SERVER_TOKEN",
        help="The Gitlab Runner registration token",
    )
    parser.add_argument(
        "--gitlab-url",
        required=True,
        metavar="CI_SERVER_URL",
        help="GitLab instance URL",
    )

    args = parser.parse_args()

    register_command = create_register_command(
        args.name,
        args.registration_token,
        args.build_concurrency,
        args.runner_concurrency,
        args.gitlab_url,
    )

    print("Running registration command:")
    print(" ".join(register_command))

    try:
        ret = subprocess.run(
            register_command, capture_output=True, check=True, text=True
        )
        print(ret.stdout)
    except subprocess.CalledProcessError as err:
        print(err.output)
        print(err.stderr)
        raise err


if __name__ == "__main__":
    main()
