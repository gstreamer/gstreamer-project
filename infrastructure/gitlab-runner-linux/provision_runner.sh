#! /usr/bin/bash

concurrent="${1:-8}"
arch="${2:-amd64}"

set -eux -o pipefail

sudo apt update -y && sudo apt full-upgrade -y

# Make sure curl is installed
# Needed for both the gitlab runner and the fd.o gating script setup
sudo apt install -y curl

# Useful utilities
sudo apt install -y htop btop vim tmux

# Install gitlab-runner and container deps
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt install -y jq podman podman-docker uidmap apparmor apparmor-utils containers-storage gitlab-runner slirp4netns dbus-user-session

# gitlab-runner is not happy with the fuse-overlay backend
sudo apt remove -y fuse-overlayfs

sudo systemctl enable --now podman.socket
sudo systemctl enable --now gitlab-runner.service

# Make sure the Podman socket remains available after the GitLab Runner user is logged out
sudo loginctl enable-linger gitlab-runner

sudo mkdir -p /var/cache/gitlab-runner/cache
sudo mkdir -p /etc/gitlab-runner

# Setup requirments for gitlab.freedesktop.org CI gating script
#
# Adapted from
# https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/blob/29f920969e4ed4ab0bcfff911e90cfe13562c5ef/cloud-init/ci/runcmd.d
sudo mkdir -p /var/host/bin

curl -L -o ~/var-curl "https://gitlab.freedesktop.org/api/v4/projects/freedesktop%2Fhelm-gitlab-infra/packages/generic/curl/7.88.1/curl-$arch"
sudo mv var-curl /var/host/bin/curl
sudo chmod +x /var/host/bin/curl

curl -L -o ~/var-jq "https://gitlab.freedesktop.org/api/v4/projects/freedesktop%2Fhelm-gitlab-infra/packages/generic/jq/jq-1.6-159-gcff5336/jq-$arch"
sudo mv var-jq /var/host/bin/jq
sudo chmod +x /var/host/bin/jq
sudo cp /etc/ssl/certs/ca-certificates.crt /var/host/

sudo cat /etc/gitlab-runner/config.toml
# Echo a value for concurrent to avoid the default value which is usally `1`
echo "concurrent = $concurrent" > sudo tee -a /etc/gitlab-runner/config.toml

# Override pids_limit for podman
# By default it's 2k after which we can't spawn any more threads
# and this causes issues in runners with high core counts
sudo mkdir -p /etc/containers/containers.conf.d/
cat << EOF | sudo tee /etc/containers/containers.conf.d/10-pids-limit.conf
[containers]
# Increase it to ~64k
pids_limit = 65536
EOF

# Set a cpuquota on machine.slice so containers always leave
# 2 cpus available to the rest of the system.
#
# journald, gitlab-runner and podman itself need cpus cycles
# so try to always make some available and avoid runner workloads
# ddosing the system.
# 
# (nproc - 4) cause hyperthreading.
# * 100 cause the format is 100% per cpu.
# Man: systemd.resource-control(5)
slice="$(($(($(nproc) - 4)) * 100))"
sudo mkdir -p /etc/systemd/system/machine.slice.d/
cat << EOF | sudo tee /etc/systemd/system/machine.slice.d/50-cpu-quota.conf
[Slice]
CPUQuota=$slice%
EOF

# Setup container and volume cleanup timer
# https://docs.gitlab.com/runner/executors/docker.html#clear-the-docker-cache
curl -o ~/clear-docker-cache https://gitlab.com/gitlab-org/gitlab-runner/-/raw/16-5-stable/packaging/root/usr/share/gitlab-runner/clear-docker-cache?ref_type=heads
chmod +x ~/clear-docker-cache
sudo mv ~/clear-docker-cache /etc/systemd/system/clear-docker-cache

cat << EOF | sudo tee /etc/systemd/system/ContainerCleanup.service
[Unit]
Description=Cleanup containers and volumes
Wants=ContainerClenup.timer

[Service]
Type=oneshot
ExecStart=/etc/systemd/system/clear-docker-cache

[Install]
WantedBy=multi-user.target
EOF

cat << EOF | sudo tee /etc/systemd/system/ContainerCleanup.timer
[Unit]
Description=Cleanup containers and volumes timer
Requires=ContainerCleanup.service

[Timer]
Unit=ContainerCleanup.service
OnCalendar=Daily

[Install]
WantedBy=timers.target
EOF

sudo systemctl enable --now ContainerCleanup.timer

# Setup image cleanup script for all the image fork tags
# This is also using the scripts from the cloud-init fd.o repo
# https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/blob/main/cloud-init/ci/files.d/usr/local/sbin/podman-free-space?ref_type=heads
sudo apt install -y python3-pip python3-git python3-yaml python3-parse python3-click
sudo pip3 install --break-system-packages podman

curl -L -o ~/podman-free-space.timer https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/cloud-init/ci/files.d/etc/systemd/system/podman-free-space.timer?ref_type=heads
curl -L -o ~/podman-free-space.service https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/cloud-init/ci/files.d/etc/systemd/system/podman-free-space.service?ref_type=heads
curl -L -o ~/podman-gc-exclude https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/cloud-init/ci/files.d/etc/podman-gc-exclude?ref_type=heads
curl -L -o ~/podman-free-space https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/cloud-init/ci/files.d/usr/local/sbin/podman-free-space?ref_type=heads

# Our runner also runs on the xiph gitlab
echo "registry.gitlab.xiph.org/xiph/.*:.*" >> ~/podman-gc-exclude

sudo mkdir -p /etc/systemd/system/
sudo mkdir -p /usr/local/sbin/

sudo mv ~/podman-free-space.service /etc/systemd/system/podman-free-space.service
sudo mv ~/podman-free-space.timer /etc/systemd/system/podman-free-space.timer
sudo mv ~/podman-gc-exclude /etc/podman-gc-exclude
sudo mv ~/podman-free-space /usr/local/sbin/podman-free-space
sudo chmod +x /usr/local/sbin/podman-free-space

sudo systemctl daemon-reload
sudo systemctl enable --now podman-free-space.timer
