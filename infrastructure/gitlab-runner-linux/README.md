# Linux Gitlab CI Runners Setup

This folder contains two scripts for setting up a linux runner that will use Podman as the job executor. The scripts assume and have only been tested on Debian.

* `provision_runner.sh` will install gitlab-runner, podman, and setup a **volume** cleanup timer, (images still need to be cleaned up manually). It will also download and setup the `/var/host/bin` path that's required for the freedesktop.org CI gating scrips. It also installs the `podman-free-space` [service](https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/blob/main/cloud-init/ci/files.d/usr/local/sbin/podman-free-space?ref_type=heads).
* `register_runner.py` is a helper script that takes care of registering a podman runner with the config we typically use.

Both of these scripts are variations of the [cloud-init](https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/tree/main/cloud-init?ref_type=heads) config that's used for the shared runners in freedesktop.org.

## Usage

Managing gitlab-runner:

```bash
gitlab-runner start/stop/status
```

The gitlab-runner configuration file is at `/etc/gitlab-runner/config.toml`

But really it's just a systemd service

```bash
systemctl status gitlab-runner
sudo journalctl -u gitlab-runner
sudo journalctl -u podman
```

To manually clean everything if we run out of space:

```bash
podman system prune --all
podman system prune --volumes
```

## Setting up a runner:

Scp the scripts into the runner and execute them:

```bash
scp infrastructure/gitlab-runner-linux/{provision_runner.sh,register_runner.py} my-new-runner:
ssh my-new-runner
bash ./provision_runner.sh
```

## Registering a runner:

To register a runner you can invoke the `register_runner.py` script like this:


## Registration

Go to the runner registration page and create a runner with the following configurations

Runner:

* System -> Linux
* tags: `docker,gstreamer`
* description: `GStreamer Runner`
* timeout: `10800` seconds


Then pass the tokens to the registration script

```bash
python3 register_runner.py --build-concurrency=8 --gitlab-url="https://gitlab.freedesktop.org" --registration-token="foobar"
```
