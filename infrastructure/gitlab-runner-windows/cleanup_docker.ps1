gitlab-runner.exe stop
Stop-Service docker

docker system prune --volumes --all
Remove-Item -Recurse -Force C:\Users\Administrator\runner\builds\

# FIXME: Check if this is still an issue with Docker 24+
# wget https://github.com/moby/docker-ci-zap/blob/master/docker-ci-zap.exe?raw=true -OutFile C:\Users\Administrator\docker-ci-zap.exe
# C:\Users\Administrator\docker-ci-zap.exe -folder C:\ProgramData\docker
