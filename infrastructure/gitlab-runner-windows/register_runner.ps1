[CmdletBinding(DefaultParameterSetName = "Standard")]
param(
    [string]
    $DockerRunnerToken,

    [string]
    $ShellRunnerToken,

    [switch]
    $Setup
)

$global:WORK_DIR = "C:/Users/Administrator/runner"
$global:CONFIG_FILE = "$global:WORK_DIR/config.toml"

function Setup {
    # Make sure docker works
    docker run --rm --isolation=process mcr.microsoft.com/windows/servercore:ltsc2022 powershell

    if (!$?) {
        Write-Host "Failed to run docker container with process isolation"
        Exit 1
    }

    New-Item $global:WORK_DIR -ItemType Directory -ea 0
    Remove-Item $global:CONFIG_FILE -ea 0

    echo "concurrent = 4" | Out-File -NoClobber -Encoding ascii -FilePath $global:CONFIG_FILE
    echo "check_interval = 11" | Out-File -Append -Encoding ascii -FilePath $global:CONFIG_FILE
    echo "" | Out-File -Append -Encoding ascii -FilePath $global:CONFIG_FILE
    echo "[session_server]" | Out-File -Append -Encoding ascii -FilePath $global:CONFIG_FILE
    echo "  session_timeout = 1800" | Out-File -Append -Encoding ascii -FilePath $global:CONFIG_FILE

    cat $global:CONFIG_FILE
}

# Useful if you need to turn on powershell tracing
# $env:RUNNER_PRE_CLONE_SCRIPT="Set-PSDebug -Trace 1; git config --global http.version 'HTTP/1.1'"
# $env:RUNNER_PRE_BUILD_SCRIPT="Set-PSDebug -Off"

# Special FDO_CI_CONCURRENT var for projects using https://gitlab.freedesktop.org/freedesktop/ci-templates/
# Usually you want it to be numcpu / concurrent jobs
#
# FIXME: Helper-image waiting for upstream support
# https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28740#note_799228814

# docker runner registration
function RegisterDocker {
    # Hard to pass an array apparently
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/37270
    # --docker-dns "[`"1.1.1.1`", `"8.8.8.8`"] `

    gitlab-runner.exe register `
        --non-interactive `
        --config "$global:CONFIG_FILE" `
        --token "$DockerRunnerToken" `
        --builds-dir "$global:WORK_DIR/builds" `
        --env "FDO_CI_CONCURRENT=16" `
        --executor "docker-windows" `
        --shell "powershell" `
        --url "https://gitlab.freedesktop.org/" `
        --docker-image "mcr.microsoft.com/windows/servercore:ltsc2022" `
        --docker-helper-image "kdeorg/gitlab-windows-runner-helper:servercoreltsc2022" `
        --docker-isolation "process" `
        --docker-tlsverify="false" `
        --docker-pull-policy "always" `
        --docker-disable-cache="true" `
        --docker-dns="1.1.1.1" `
        --docker-privileged="false"

    if (!$?) {
        Write-Host "Failed to register docker runner"
        Exit 1
    }
}

# Shell runner registration
function RegisterShell {
    gitlab-runner.exe register `
        --non-interactive `
        --config "$global:CONFIG_FILE" `
        --token "$ShellRunnerToken" `
        --builds-dir "$global:WORK_DIR/shell-builds" `
        --env "FDO_CI_CONCURRENT=16" `
        --executor "shell" `
        --shell "powershell" `
        --url "https://gitlab.freedesktop.org/"

    if (!$?) {
        Write-Host "Failed to register shell runner"
        Exit 1
    }
}

# Install the runner service
function InstallService {
    gitlab-runner install `
        --working-directory="$global:WORK_DIR" `
        --config="$global:CONFIG_FILE"

    if (!$?) {
        Write-Host "Failed to install gitlab-runner service"
        Exit 1
    }
}

if ($Setup) {
    Setup
}

if ($DockerRunnerToken) {
    RegisterDocker
}

if ($ShellRunnerToken) {
    RegisterShell
}

if ($DockerRunnerToken -or $ShellRunnerToken) {
    InstallService
}
