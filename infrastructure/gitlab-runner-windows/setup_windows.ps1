# It tries to scan gitlab-runner's build artifacts and tanks everything
Uninstall-WindowsFeature Windows-Defender

if (!$?) {
    Write-Host "Failed to remove Windows Defender"
    Exit 1
}

# Install chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

if (!$?) {
    Write-Host "Failed to install chocolatey"
    Exit 1
}

choco install -y git --params "/NoAutoCrlf /NoCredentialManager /NoShellHereIntegration /NoGuiHereIntegration /NoShellIntegration"

if (!$?) {
    Write-Host "Failed to install git"
    Exit 1
}

choco install -y gitlab-runner notepadplusplus

if (!$?) {
    Write-Host "Failed to install gitlab-runner or notepadplusplus"
    Exit 1
}

# Enalbe Long path
# https://learn.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation?tabs=powershell
New-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem" -Name "LongPathsEnabled" -Value 1 -PropertyType DWORD -Force

# Change the TCP profile
# https://techcommunity.microsoft.com/t5/networking-blog/tcp-templates-for-windows-server-2019-8211-how-to-tune-your/ba-p/339795
Set-NetTCPSetting -SettingName Datacenter

# Add docker volume cleanup scheduled task
$action = New-ScheduledTaskAction -Execute "docker system prune --volumes"
$trigger = New-ScheduledTaskTrigger -Weekly -WeeksInterval 1 -DaysOfWeek Sunday -At 3am
Register-ScheduledTask VolumeCleanup -Action $action -Trigger $trigger

if (!$?) {
    Write-Host "Failed to register cleanup task"
    Exit 1
}

# Install docker
# https://learn.microsoft.com/en-us/virtualization/windowscontainers/quick-start/set-up-environment?tabs=dockerce
Invoke-WebRequest  -o install-docker-ce.ps1 -UseBasicParsing "https://raw.githubusercontent.com/microsoft/Windows-Containers/Main/helpful_tools/Install-DockerCE/install-docker-ce.ps1"

if (!$?) {
    Write-Host "Failed to download docker install script"
    Exit 1
}

.\install-docker-ce.ps1 -NoRestart

if (!$?) {
    Write-Host "Failed to install docker"
    Exit 1
}

# Required for Docker and Windows defener to function
Restart-Computer
