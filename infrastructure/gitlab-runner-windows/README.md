# Windows Runner Setup

## Windows Setup

To setup a Windows VM look at `setup_ubuntu_vm_host.sh` to setup the VM

```
culr -o ~/setup_ubuntu_vm_host.sh  "https://gitlab.freedesktop.org/gstreamer/gstreamer-project/-/raw/main/infrastructure/gitlab-runner-windows/setup_ubuntu_vm_host.sh"
bash ~/setup_ubuntu_vm_host.sh
```

This will create a VM and a VNC server on local host, which you will need to connect to do the windows installation. Once on the install screen, select Windows Datacenter Desktop Edition.
By default it will not find a disk drive to install, however we can load the virtio driver from the iso. Click Load Driver -> Browse -> Find the VirtIO cd drive -> and select the folder under `amd64/2k22`.

Once the windows install setup is done, you will have no internet. You will need to open the VirtIO cdrom drive in explorer, and instal the guest tools.

Optionally, enable RDP and login through that so you can copy paste the next step and manage the machine more easily.
To do this you will have to find the IP of the interface from the network settings in Windows (or similar), and then use that from the host to connect to it. So assuming you've already sshed into the host, you will have then to connect to something like: `192.168.122.223:3389`

Afterwards you can find the windows guest setup in `setup_windows.ps1`

```
Invoke-WebRequest -o ~/setup_windows.ps1 -UseBasicParsing "https://gitlab.freedesktop.org/gstreamer/gstreamer-project/-/raw/main/infrastructure/gitlab-runner-windows/setup_windows.ps1"
~/setup_windows.ps1
```

## Registration

Go to the runner registration page and create 2 runners with the following configurations 

Docker Runner:

* System -> Windows
* tags: `docker,windows,2022,gstreamer-windows,mesa`
* description: `Windows 2022 docker runner provided by the GStreamer Foundation`
* timeout: `10800` seconds

Shell Runner:

* System -> Windows
* tags: `shell,windows,2022,gstreamer-windows,mesa`
* description: `Windows 2022 powershell runner provided by the GStreamer Foundation`
* timeout: `10800` seconds

Then pass the tokens to the registration script

```
Invoke-WebRequest -o ~/register_runner.ps1 -UseBasicParsing "https://gitlab.freedesktop.org/gstreamer/gstreamer-project/-/raw/main/infrastructure/gitlab-runner-windows/register_runner.ps1"
./register_runner.ps1 -Setup
./register_runner.ps1 -DockerRunnerToken "abc" -ShellRunnerToken "abc"
```
